#include "dialog.h"
#include "ui_category_dialog.h"

/** \namespace dialog::category */
namespace dialog::category {

Dialog::Dialog(QString const& title, QString const& title_item
     , QString const& category_title, QWidget* parent)
    : inherited(parent)
    , ui(std::make_unique<instance_t>(name(), title_item))
{
    ui->instance(this, title);
    setCategoryTitle(category_title);
    dialog_t::init(this, name(), title, {{this, default_t::Min}});
}
Dialog::~Dialog()
{
    dialog_t::settingsWrite();
}
 QString Dialog::name() noexcept
 {
     return QStringLiteral("CategoryDialog");
 }
void Dialog::setCategoryTitle(QString const& title) noexcept
{
    (*ui)->l_category->setText(title);
}
bool Dialog::isValid() const noexcept
{
    return  !(*ui)->m_category->text().isEmpty();
}
void Dialog::onDataChanged()
{
    inherited::doDataChanged((*ui)->m_buttons);
}
QString Dialog::category() const noexcept
{
    return (*ui)->m_category->text();
}
QString Dialog::item() const noexcept
{
    return category();
}

} // end namespace dialog::category
