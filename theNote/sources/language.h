/**
 * \file      c:/projects/thenote/theNote/main/language.h 
 * \brief     The Language class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2018 
 * \version   v.1.0
 * \created   July      (the) 12(th), 2018, 00:51 MSK
 * \updated   July      (the) 12(th), 2018, 00:51 MSK
 * \TODO      
**/
#pragma once
#include <array>
#include <algorithm>
#include <QLocale>
#include <QComboBox>
#include <QStringList>
#include <ps2/qt/out.h>
#include "sources/settings/settings.h"

/** begin namespace options */
namespace options {

/**
 * \code
 *    auto&& lang = options::Language::currentBcp47Name();
 *    auto&& lang = options::Language::current();
 *    auto&& langs = options::Language::defaultLanguages();
 *    options::Language::fill(combobox);
 *    auto const result = ui->m_language->findData(lang_id);
 *    m_language = language_t(ui->m_language->currentData().toUInt());
 * \endcode
**/
class Language final {
public:
    using class_name  = Language;
    using id_t        = uint;
    using language_t  = QLocale::Language;
    using languages_t = std::array<language_t,2>;

private:
    language_t m_language = QLocale::AnyLanguage;

private:
    static inline QString path() noexcept {
        return QStringLiteral("options/lang");
    }
    static inline bool isValidLanguage(language_t lang) noexcept {
        return     (lang == QLocale::English)
                || (lang == QLocale::Russian);
    }
    static inline QString navName(language_t lang) noexcept {
        return QLocale(lang).nativeLanguageName();
    }
    static inline void addItem(QComboBox* box, language_t lang) noexcept {
        box->addItem(navName(lang), static_cast<id_t>(lang));
    }

public:
    explicit Language() noexcept
        : m_language(current().lang()) {
    }
    Q_DECL_CONSTEXPR explicit Language(language_t lang) noexcept
        : m_language(lang){
    }
    Q_DECL_CONSTEXPR explicit Language(uint lang) noexcept
        : m_language(static_cast<language_t>(lang)){
    }
    ~Language() Q_DECL_EQ_DEFAULT;
    friend bool operator==(class_name const& lhs, class_name const& rhs) noexcept {
        return lhs.m_language == rhs.m_language;
    }
    friend bool operator!=(class_name const& lhs, class_name const& rhs) noexcept {
        return !(lhs.m_language == rhs.m_language);
    }
    friend bool operator==(class_name const& lhs, language_t rhs) noexcept {
        return lhs.m_language == rhs;
    }
    friend bool operator!=(class_name const& lhs, language_t rhs) noexcept {
        return !(lhs.m_language == rhs);
    }
    Q_DECL_CONSTEXPR language_t lang() const noexcept {
        return m_language;
    }
    Q_DECL_CONSTEXPR uint toUint() const noexcept {
        return static_cast<uint>(m_language);
    }
    Q_DECL_CONSTEXPR void setLanguage(language_t lang) noexcept {
        m_language = lang;
    }
    bool isValid() const noexcept {
        return isValidLanguage(m_language);
    }
    QString bcp47Name() const noexcept {
        return QLocale(m_language).bcp47Name();
    }
    static inline QString currentBcp47Name() noexcept {
        return class_name(current()).bcp47Name();
    }
    static inline class_name current() noexcept {
        auto const& val = Settings::val(path(), static_cast<uint>(defaultLanguage().lang()));
        return isValidLanguage(static_cast<QLocale::Language>(val.toUInt()))
                ? class_name(val.toUInt())
                : defaultLanguage();
    }
    static inline bool isCurrentEnglish() noexcept {
        return current().lang() == QLocale::English;
    }
    static inline class_name english() noexcept {
        return class_name{QLocale::English};
    }
    static inline void setCurrent(language_t lang) noexcept {
        Settings::setVal(path(), static_cast<uint>(lang));
    }
    static inline void setCurrent(class_name const& lang) noexcept {
        setCurrent(lang.lang());
    }
    static inline class_name defaultLanguage() noexcept {
        auto const lang = QLocale().language();
        auto const result = isValidLanguage(lang) ? lang : QLocale::English;
        return class_name(result);
    }
    static inline languages_t defaultLanguages() noexcept {
        return {{
            QLocale::English
          , QLocale::Russian
        }};
    }
    static inline QStringList defaultLanguagesNames() noexcept {
        QStringList result;

        foreach(auto lang, defaultLanguages())
            result << navName(lang);

        return result;
    }
    static inline void fill(QComboBox* box) noexcept {
        foreach(auto c, defaultLanguages())
            addItem(box, c);
    }
    void swap(Language& rhs) noexcept {
        qSwap(m_language, rhs.m_language);
    }
};
static inline void swap(Language& lhs, Language& rhs) noexcept {
    lhs.swap(rhs);
}
inline QDebug operator<<(QDebug dbg, Language const& v) noexcept {
    QString lang = v.lang() == QLocale::Russian ? QStringLiteral("English")
                                                : QStringLiteral("Russian");
    return dbg.nospace()
        << "\"Language\": {"
        <<  "\"id\":" << v.lang()
        <<  ", \"name\":" << lang
        << "}";
}
inline std::ostream& operator<<(std::ostream& out, options::Language const& lang) noexcept {
    return ps2::cout(out, lang);
}

} // end namespace options

inline uint currentLanguageId() noexcept {
    return options::Language::current().toUint();
}
inline uint englishId() noexcept {
    return options::Language::english().toUint();
}

Q_DECLARE_METATYPE(options::Language)
