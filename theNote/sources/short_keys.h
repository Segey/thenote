/**
 * \file      c:/projects/thenote/theNote/main/short_keys.h 
 * \brief     The Short_keys class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2018 
 * \version   v.1.0
 * \created   July      (the) 30(th), 2018, 23:04 MSK
 * \updated   July      (the) 30(th), 2018, 23:04 MSK
 * \TODO      
**/
#pragma once
#include <QKeySequence>
#include "tr.h"

class ShortKey final {
public:
    using class_name = ShortKey;
    using key_t = QKeySequence;

public:
    static key_t create() noexcept {
        return QKeySequence::New;
    }
    static key_t open() noexcept {
        return QKeySequence::Open;
    }
    static key_t save() noexcept {
        return QKeySequence::Save;
    }
    static key_t saveAs() noexcept {
        return QKeySequence::SaveAs;
    }
    static key_t edit() noexcept {
        return QKeySequence(Qt::CTRL + Qt::Key_I);
    }
    static key_t print() noexcept {
        return QKeySequence::Print;
    }
    static key_t exit() noexcept {
        return QKeySequence::Close;
    }
    static key_t quit() noexcept {
        return QKeySequence::Quit;
    }
    static key_t cut() noexcept {
        return QKeySequence::Cut;
    }
    static key_t copy() noexcept {
        return QKeySequence::Copy;
    }
    static key_t paste() noexcept {
        return QKeySequence::Paste;
    }
    static key_t clear() noexcept {
        return QKeySequence::Delete;
    }
    static key_t undo() noexcept {
        return QKeySequence::Undo;
    }
    static key_t redo() noexcept {
        return QKeySequence::Redo;
    }
    static key_t help() noexcept {
        return QKeySequence::HelpContents;
    }
    static key_t bold() noexcept {
        return QKeySequence::Bold;
    }
    static key_t italic() noexcept {
        return QKeySequence::Italic;
    }
    static key_t underline() noexcept {
        return QKeySequence::Underline;
    }
    static key_t alignLeft() noexcept {
        return Qt::CTRL + Qt::Key_L;
    }
    static key_t alignRight() noexcept {
        return Qt::CTRL + Qt::Key_R;
    }
    static key_t alignCenter() noexcept {
        return Qt::CTRL + Qt::Key_E;
    }
    static key_t alignJustify() noexcept {
        return Qt::CTRL + Qt::Key_J;
    }
    static key_t selectAll() noexcept {
        return QKeySequence::SelectAll;
    }
    static key_t unknown() noexcept {
        return QKeySequence::UnknownKey;
    }
};
