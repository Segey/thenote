/**
 * \file      c:/projects/thenote/theNote/sources/settings/last_settings.h 
 * \brief     The Last_settings class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   October   (the) 18(th), 2019, 00:05 MSK
 * \updated   October   (the) 18(th), 2019, 00:05 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include "settings.h"

/** \namespace settings */
namespace settings {

/** \namespace empty */
namespace {
        
static inline QString last(QString const& name) noexcept {
    return QStringLiteral("last/%1").arg(name);
}
static inline QString last(QString&& name) noexcept {
    return QStringLiteral("last/%1").arg(qMove(name));
}

} // end empty namespace

/**
 * \code
        auto const& file = QFileDialog::getOpenFileName(this, iTitle::tr("Open an image")
                                        , settings::lastImageFile(), FileTypes::imageFiles());
        if(!file.isEmpty())
            settings::setLastImageFile(file);
 * \endcode
**/
static inline void setLastImageFile(QString&& path) noexcept {
    Settings::setVal(last(QStringLiteral("image")), qMove(path));
}
static inline void setLastImageFile(QString const& path) noexcept {
    Settings::setVal(last(QStringLiteral("image")), path);
}
static inline QString lastImageFile() noexcept {
    return Settings::val(last(QStringLiteral("image"))).toString();
}

} // end namespace settings
