/**
 * \file      c:/projects/thenote/theNote/main/tr.h 
 * \brief     The Tr class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2018 
 * \version   v.1.0
 * \created   July      (the) 12(th), 2018, 00:33 MSK
 * \updated   July      (the) 12(th), 2018, 00:33 MSK
 * \TODO      
**/
#pragma once
#include <QObject>

#define TR_CLASS(cls) class cls : public QObject { Q_OBJECT };

TR_CLASS(iThe)
TR_CLASS(iButton)
TR_CLASS(iCopyright)
TR_CLASS(iMenu)
TR_CLASS(iTitle)
