#include "sources/app.h"
#include "sources/tr.h"
#include "thenote.h"
#include "CategoryDialog/add_dialog.h"
//#include <main/action.h>
//#include <main/program.h>
//#include <main/pum/pum.h>
//#include <main/message_box.h>
//#include "simple_records_manager.h"
#include "ui_thenote.h"
//#include "simple_records_manager_instance.h"
//#include <main/models/view/full/date.h>
//#include <main/db/maindb_source/records/simple_record/simple_record_reader.h>
//#include <main/db/maindb_source/records/simple_record/simple_record_writer.h>
//#include <main/models/view/list_model.h>
//#include <main/models/table/simple_table_model.h>
//#include <main/models/handlers/handlers.h>
//#include "SimpleRecords/SimpleRecordsDialog/classes/item_dialog/add_item_dialog.h"
//#include "SimpleRecords/SimpleRecordsDialog/classes/item_dialog/edit_item_dialog.h"
//#include "SimpleRecords/SimpleRecordsDialog/add_dialog.h"
//#include "SimpleRecords/SimpleRecordsDialog/edit_dialog.h"

Manager::Manager(QWidget *parent) noexcept
    : QMainWindow(parent)
    , ui(std::make_unique<ui_t>())
{
    ui->setupUi(this);
    //ManagerInstance<class_name, ui_t>().instance(this, ui.get());
    instanceViewModel();
    //instanceTableModel();

    //main_window::init(this, QStringLiteral("SimpleRecordsManager")
                      //, iTitle::tr("Records History")
                      //, {{this, ps2::Fraction3{2}}
                      //, {QStringLiteral("table"), ui->m_table}
                    //});

    instanceSignals();
    onUpdate();
}
Manager::~Manager()
{
    //main_window::settingsWrite();
}
bool Manager::isSelected() const noexcept
{
    //return mh::isSelected(ui->m_view);
    return true;
}
void Manager::selectFirstRow() noexcept
{
    //if(isSelected() || m_view.rowCount() < 1) return;

    //ui->m_view->setCurrentIndex(m_view.index(0));
    //ui->m_view->clicked(m_view.index(0));
}
void Manager::instanceTableModel() noexcept
{
    //m_table.setParent(this);
    //ui->m_table->setModel(&m_table);
}
void Manager::instanceViewModel() noexcept
{
    m_view.setParent(this);
    //m_view.init(reader_t::readAllViews(MAIN_DB));
    m_view.init();
    ui->m_view->setHeaderHidden(true);
    ui->m_view->setModel(&m_view);
}
void Manager::instanceSignals() noexcept
{
    connect(&m_view, &QAbstractItemModel::rowsInserted, this,  &Manager::onUpdate);
    connect(&m_view, &QAbstractItemModel::rowsRemoved, this,   &Manager::onUpdate);
    connect(&m_view, &QAbstractItemModel::dataChanged, this, &Manager::onUpdate);
    connect(ui->m_view->selectionModel(), &QItemSelectionModel::currentChanged, this
                , &Manager::onSelect);

    //PUM_DAYS(ui->m_view
             //, iTitle::tr("Add a record day...")
             //, iTitle::tr("Edit the record day...")
             //, iTitle::tr("Delete the record day")
             //, iTitle::tr("Remove all records days"))

    connect(ui->m_view, &QWidget::customContextMenuRequested, [this](QPoint const& pos) {
        QMenu myMenu;
        myMenu.addAction(iMenu::tr("Add a category"), this, SLOT(onAddCategory()));
        myMenu.addAction(iMenu::tr("Add an item"), this, SLOT(onAddItem()));
        myMenu.exec(ui->m_view->mapToGlobal(pos));

        //auto const is =  mh::isSelected(ui->m_table);
        //pum::Pum({
              //pum::Item{ico16::document(), iTitle::tr("Add a record...")
                //, std::bind(static_cast<void (class_name::*)()>(&class_name::onAddItem), this), isSelected()}
            //, pum::Item{ico16::document_edit(), iTitle::tr("Edit the record...")
                //, std::bind(static_cast<void (class_name::*)()>(&class_name::onEditItem), this), is}
            //, pum::Item{ico16::document_remove(), iTitle::tr("Delete the record")
                //, std::bind(static_cast<void (class_name::*)()>(&class_name::onRemoveItem), this), (is && m_table.rowCount() > 1)}
        //}, this).exec(ui->m_table->mapToGlobal(pos));
    });
}
void Manager::onSelect()
{
    qDebug() << "OnSelect";
}
void Manager::onUpdate()
{
    qDebug() << "Cool";
    //selectFirstRow();

    //auto const is = isSelected();
    //ui->m_edit->setEnabled(is);
    //ui->m_remove->setEnabled(is);
    //ui->m_remove_all->setEnabled(is);
}
void Manager::onAddCategory() noexcept
{
    dialog::category::AddDialog w(QStringLiteral("Add Muscles")
               , QStringLiteral("a Muscle"), iTitle::tr("&Category2:"));
    if(w.exec() == QDialog::Accepted)
        qDebug() << w.item();
}
void Manager::onEditDay() noexcept
{
    //auto&& list = reader_t::readAllKeys(MAIN_DB);
    //if(mhmd::edit<EditDialog, writer_t, reader_t, full_view_t>(ui->m_view, m_view, this, list))
        //onViewModelClicked(mh::selected(ui->m_view));
}
void Manager::onRemoveDay() noexcept
{
    //if(mhmd::remove<writer_t, full_view_t>(ui->m_view, m_view, this))
        //onViewModelClicked(mh::selected(ui->m_view));
}
void Manager::onRemoveAllDays() noexcept
{
    //if(mhmd::removeAll<writer_t>(m_view, this))
        //onViewModelClicked({});
}
void Manager::onAddItem() noexcept
{
    //auto&& keys = reader_t::readAllKeys(MAIN_DB);
    //mhmi::add2<AddItemDialog, writer_t, full_view_t>(ui.get(), m_table, m_view, this, keys);
}
void Manager::onEditItem() noexcept
{
    //auto&& keys =  reader_t::readAllKeys(MAIN_DB);
    //mhmi::edit<EditItemDialog, writer_t>(ui.get(), m_table, m_view, this, keys);
}
void Manager::onRemoveItem() noexcept
{
//    mhmi::remove<writer_t>(ui.get(), m_table, m_view, tDaDaDayyyhis)Day;
}
void Manager::onViewModelClicked(QModelIndex const& /*index*/) noexcept
{
    //if(!index.isValid()) {
        //m_table.init({});
        //return;
    //}

    //auto const& item = mhv::item<full_view_t>(m_view, index);
    //m_table.init(toVector(item->items()));
}
