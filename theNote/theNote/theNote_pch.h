/**
 * \file      perfect/Records/SimpleRecords/SimpleRecordsManager/simple_records_manager_pch.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   September (the) 29(th), 2015, 00:39 MSK
 * \updated   September (the) 29(th), 2015, 00:39 MSK
 * \TODO      
**/
#include <memory>
#include <QDate>
#include <QtGui>
#include <QDebug>
#include <QtCore>
#include <QAction>
#include <QObject>
#include <QString>
#include <QWidget>
#include <QVariant>
#include <QSqlQuery>
#include <QMainWindow>
#include <QModelIndex>
#include <QStringList>
#include <QTranslator>
#include <QApplication>
