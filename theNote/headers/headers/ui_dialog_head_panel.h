/**
 * \file      perfect/Headers/headers/ui_dialog_head_panel.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2018
 * \version   v.1.0
 * \created   January (the) 13(th), 2016, 19:07 MSK
 * \updated   January (the) 13(th), 2016, 19:07 MSK
 * \TODO
**/
#pragma once
#include <QWidget>
#include <QPixmap>
#include <QString>
#include <QObject>
#include "../headers_global.h"

/** \namespace ui */
namespace ui {

namespace instance {
    class DialogHeadPanel;
}

class DialogHeadPanelInstance;

class HEADERS_SHARED_EXPORT DialogHeadPanel : public QWidget {
public:
    using class_name = DialogHeadPanel;
    using inherited  = QWidget;
    using instance_t = instance::DialogHeadPanel;

private:
    QPixmap     m_pixmap;
    QString     m_caption;
    QString     m_title;
    instance_t* ui = Q_NULLPTR;

private:
    void instanceAfter();

public:
    explicit DialogHeadPanel(QWidget* parent, QPixmap const& pixmap, QString const& caption, QString const& title);
    explicit DialogHeadPanel(QPixmap const& pixmap, QString const& caption, QString const& title);
    virtual ~DialogHeadPanel() Q_DECL_OVERRIDE;
    void instance(QWidget* parent = Q_NULLPTR);
    QPixmap const& pixmap() const;
    void setIcon(QPixmap const& pixmap) noexcept;
    QString const& title() const;
    void setTitle(QString const& title);
    QString const& caption() const;
    void setCaption(QString const& caption) noexcept;
    void setCaption(QString&& caption) noexcept;
};

} // end namespace ui
