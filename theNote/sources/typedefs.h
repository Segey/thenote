/**
 * \file      c:/projects/thenote/theNote/main/typedefs.h 
 * \brief     The Typedefs class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2018 
 * \version   v.1.0
 * \created   July      (the) 30(th), 2018, 23:01 MSK
 * \updated   July      (the) 30(th), 2018, 23:01 MSK
 * \TODO      
**/
#pragma once
#include <numeric>
#include <ps2/cpp/typedefs.h>

/**
 * \code
 *      THE_UI(OptionsDialog)
 * \endcode
**/
#define THE_UI(ui) namespace Ui { class ui; }
