#include "add_dialog.h"
#include "ui_category_dialog.h"

/** \namespace dialog::category */
namespace dialog::category {

bool AddDialog::doIsChanged() const
{
    return !(*ui)->m_category->text().isEmpty();
}
bool AddDialog::doIsValid() const
{
    return inherited::isValid();
}

} // end namespace dialog::category
