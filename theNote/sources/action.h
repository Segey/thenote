/**
 * \file      c:/projects/thenote/theNote/main/action.h 
 * \brief     The Action class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2018 
 * \version   v.1.0
 * \created   July      (the) 30(th), 2018, 23:03 MSK
 * \updated   July      (the) 30(th), 2018, 23:03 MSK
 * \TODO      
**/
#pragma once
#include <functional>
#include <QIcon>
#include <QAction>
#include <QUndoStack>
#include <QActionGroup>
#include <QKeySequence>
#include "tr.h"
#include "consts.h"
#include "short_keys.h"

class Action {
    using class_name = Action;

public:
    template<class T>
    static QActionGroup* makeGroup(T const& list, QWidget* parent, int index = -1) {
        auto result = new QActionGroup(parent);
        std::for_each(list.begin(), list.end(), std::bind( static_cast<QAction* ( QActionGroup::* )(QAction*)>(&QActionGroup::addAction), result, std::placeholders::_1));
        std::for_each(list.begin(), list.end(), std::bind( (&QAction::setCheckable),std::placeholders::_1, true));
        if (0 <= index && index < list.size()) list[index]->setCheckable(true);
        return result;
    }
    static QAction* create(QIcon const& icon, QString const& text, QString const& tooltip, QObject* parent, QKeySequence const& key) {
            auto action = new QAction(icon, text, parent);
            action->setShortcut(key);
            if( key == QKeySequence(QKeySequence::UnknownKey) ) action->setToolTip(tooltip);
                else action->setToolTip(QStringLiteral("%1 (%2)").arg(tooltip, key.toString()));
        return action;
    }
    static QAction* createAbout(QObject* parent) {
        return create(ico32::about(), iButton::tr("&About...")
                  , iButton::tr("About"), parent, ShortKey::unknown());
    }
    static QAction* createCalcAbout(QObject* parent) {
        return create(ico32::about(), iButton::tr("&About calculator..."), iButton::tr("About the calculator"), parent, ShortKey::unknown());
    }
    static QAction* createExit(QObject* parent) {
         return create(ico32::exit(), iButton::tr("Clos&e"), iButton::tr("Close"), parent, ShortKey::exit());
    }
    static QAction* createCut(QObject* parent) {
        return create(ico32::cut(), iButton::tr("C&ut"), iButton::tr("Cut"), parent, ShortKey::cut());
    }
    static QAction* createCutWithTip(QObject* parent) {
        auto action = createCut(parent);
        action->setStatusTip(iButton::tr("Cut the current selection's contents"));
        return action;
    }
    static QAction* createCopy(QObject* parent) {
        return  create(ico32::copy(), iButton::tr("&Copy"), iButton::tr("Copy"), parent, ShortKey::copy());
    }
    static QAction* createCopyWithTip(QObject* parent) {
        auto action = createCopy(parent);
        action->setStatusTip(iButton::tr("Copy the current selection's contents"));
        return action;
    }
    static QAction* createPaste(QObject* parent) {
        return create(ico32::paste(), iButton::tr("&Paste"), iButton::tr("Paste"), parent, ShortKey::paste());
    }
    static QAction* createPasteWithTip(QObject* parent) {
        auto action = createPaste(parent);
        action->setStatusTip(iButton::tr("Paste the clipboard's contents"));
        return action;
    }
    static QAction* createClear(QObject* parent) {
        return create(ico32::clear(), iButton::tr("C&lear"), iButton::tr("Clear"), parent, ShortKey::clear());
    }
    static QAction* createClearText(QObject* parent) {
        auto action = createClear(parent);
        action->setStatusTip(iButton::tr("Completely clears the document contents"));
        return action;
    }
    static QAction* createUndo(QObject* parent, QUndoStack* stack) noexcept {
        auto action = stack->createUndoAction(parent, iButton::tr("U&ndo"));
        action->setShortcut(ShortKey::undo());
        action->setIcon(ico32::undo());
        return action;
    }
    static QAction* createUndo(QObject* parent) noexcept {
        return create(ico32::undo(), iButton::tr("U&ndo"), iButton::tr("Undo"), parent, ShortKey::undo());
    }
    static QAction* createRedo(QObject* parent, QUndoStack* stack) {
        auto action = stack->createRedoAction(parent, iButton::tr("Re&do"));
        action->setShortcut(ShortKey::redo());
        action->setIcon(ico32::redo());
        return action;
    }
    static inline QAction* createRedo(QObject* parent) noexcept {
        return create(ico32::redo(), iButton::tr("Re&do")
                      , iButton::tr("Redo"), parent, ShortKey::redo());
    }
    static inline QAction* createHelp(QObject* parent) noexcept {
        return create(ico32::help(), iButton::tr("&Help...")
                  , iButton::tr("Help Contents"), parent, ShortKey::help());
    }
    static inline QAction* createShortHelp(QObject* parent) noexcept {
        return create(ico32::help(), iButton::tr("&Help Contents...")
                  , iButton::tr("Help Contents"), parent, ShortKey::help());
    }
    static QAction* createRegistration(QObject* parent) {
        return create(QIcon(), iCopyright::tr("&Registration..."), iCopyright::tr("Registration"), parent, ShortKey::unknown());
    }
    static inline QAction* createHomePage(QObject* parent) noexcept {
        return create(QIcon(), iButton::tr("Ho&me Page...")
                  , iButton::tr("Home Page"), parent, ShortKey::unknown());
    }
    static inline QAction* createColor(QObject* parent) noexcept {
        return create(QIcon(), iButton::tr("Create& a color...")
              , iButton::tr("Create a new color"), parent, ShortKey::unknown());
    }
};
