/**
 * \file      perfect/Headers/headers_pch.h
 * \brief     The Headers_pch class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2018
 * \version   v.1.0
 * \created   February  (the) 06(th), 2017, 16:04 MSK
 * \updated   February  (the) 06(th), 2017, 16:04 MSK
 * \TODO      
**/
#include <memory>
#include <QIcon>
#include <QFrame>
#include <QLabel>
#include <QObject>
#include <QPixmap>
#include <QString>
#include <QWidget>
#include <QComboBox>
#include <QGroupBox>
#include <QClipboard>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QToolButton>
#include <QVBoxLayout>
#include <QTextBrowser>
#include <QtCore/qglobal.h>
