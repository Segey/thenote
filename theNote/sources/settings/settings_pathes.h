/**
 * \file      thenote/theNote/sources/settings/settings_pathes.h 
 * \brief     The Settings_pathes class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   October   (the) 18(th), 2019, 00:04 MSK
 * \updated   October   (the) 18(th), 2019, 00:04 MSK
 * \TODO      
**/
#pragma once
#include <QString>

/** \namespace settings_pathes */   
namespace settings_pathes {

inline QString panel(QString const& name) noexcept {
    return QStringLiteral("panels/%1/").arg(name);
}
inline QString panel(QString&& name) noexcept {
    return QStringLiteral("panels/%1/").arg(qMove(name));
}
inline QString panel(QString const& name, QString const& value) noexcept {
    return QStringLiteral("panels/%1/%2").arg(name, value);
}
inline QString panel(QString&& name, QString&& value) noexcept {
    return QStringLiteral("panels/%1/%2").arg(qMove(name), qMove(value));
}
inline QString dialog(QString const& name) noexcept {
    return QStringLiteral("dialogs/%1/").arg(name);
}
inline QString dialog(QString&& name) noexcept {
    return QStringLiteral("dialogs/%1/").arg(qMove(name));
}
/**
 * \code
 *      auto const& group = settings_pathes::dialog(name());
 * \endcode
**/
inline QString dialog(QString const& name, QString const& value) noexcept {
    return QStringLiteral("dialogs/%1/%2").arg(name, value);
}
inline QString dialog(QString&& name, QString&& value) noexcept {
    return QStringLiteral("dialogs/%1/%2").arg(qMove(name), qMove(value));
}
inline QString window(QString const& name) noexcept {
    return QStringLiteral("windows/%1/").arg(name);
}
inline QString window(QString&& name) noexcept {
    return QStringLiteral("windows/%1/").arg(qMove(name));
}
inline QString window(QString const& name, QString const& value) noexcept {
    return QStringLiteral("windows/%1/%2").arg(name, value);
}
inline QString window(QString&& name, QString&& value) noexcept {
    return QStringLiteral("windows/%1/%2").arg(qMove(name), qMove(value));
}

} // end namespace settings_pathes
