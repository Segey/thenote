#-------------------------------------------------
#
# Project created by QtCreator 2019-11-15T22:55:38
#
#-------------------------------------------------
TEMPLATE            = lib
QT                 += widgets sql core
TARGET              = categorydlg
DEFINES            += CATEGORY_DIALOG_LIBRARY
PRECOMPILED_HEADER  = category_dialog_pch.h

include(../../theNote.pri)

MY_LIBS = -lheaders

CONFIG(debug, debug|release) {
} else {
    CONFIG  -= console
}

LIBS += -L$$DESTDIR $$MY_LIBS

HEADERS += \
    dialog.h \
    add_dialog.h \
    dialog_instance.h \
    category_dialog_global.h \
    edit_dialog.h \

SOURCES += \
    dialog.cpp \
    add_dialog.cpp \
    edit_dialog.cpp

FORMS += \
    category_dialog.ui
