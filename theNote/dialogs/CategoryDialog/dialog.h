/**
 * \file      theNote/dialogs/category/dialog.h 
 * \brief     The Dialog class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   October   (the) 15(th), 2019, 22:53 MSK
 * \updated   October   (the) 15(th), 2019, 22:53 MSK
 * \TODO      
**/
#pragma once
#include <memory>
#include <QVector>
#include <QString>
#include <QVariant>
#include "sources/widgets/window/add_edit_dialog.h"
#include "dialog_instance.h"
#include "category_dialog_global.h"

THE_UI(CategoryDialog)

/** \namespace dialog::category */
namespace dialog::category {

class CATEGORY_DIALOG_SHARED_EXPORT Dialog: public dialog::AEDialog {
    Q_OBJECT

friend class DialogInstance<Ui::CategoryDialog, Dialog>;

public:
    using class_name  = Dialog;
    using inherited   = dialog::AEDialog;
    using instance_t  = DialogInstance<Ui::CategoryDialog, Dialog>;

private:
    static QString name() noexcept;

protected:
    std::unique_ptr<instance_t> ui;

protected:
    bool isValid() const noexcept;

private slots:
    void onDataChanged();

public:
   /**
     * \code
        dialog::category::EditDialog w(QStringLiteral("Add Muscles")
                   , QStringLiteral("a Muscle"), iTitle::tr("&Category2:"));
        w.setCategory(QStringLiteral("Cool"));
        if(w.exec() == QDialog::Accepted)
            qDebug() << w.item();
     * \endcode
    **/
    explicit Dialog(QString const& title, QString const& title_item
                 , QString const& category_title, QWidget* parent = Q_NULLPTR);
    virtual ~Dialog();
    void setCategoryTitle(QString const& title) noexcept;
    QString category() const noexcept;
    QString item() const noexcept;
};

} // end namespace dialog::category
