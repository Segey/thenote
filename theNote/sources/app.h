﻿/**
 * \file      c:/projects/thenote/theNote/main/app.h 
 * \brief     The App class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2018 
 * \version   v.1.0
 * \created   July      (the) 12(th), 2018, 00:33 MSK
 * \updated   July      (the) 12(th), 2018, 00:33 MSK
 * \TODO      
**/
#pragma once
#include <QDebug>
#include <QTranslator>
#include <QApplication>
#include "sources/program.h"
#include "sources/settings/settings.h"
#include "sources/log.h"
#include "sources/language.h"

/**
 * \code
 *     auto const& val = App::settings()->value(QStringLiteral("options/maindb"), Program::mainDb());
 *     App::exit();
 * \endcode
**/

class App final: public QApplication  {
public:
    using class_name = App;
    using inherited  = QApplication;

private:
    bool notify(QObject *receiver, QEvent *event) noexcept {
        try {
            return QApplication::notify(receiver, event);
        } catch (std::exception &e) {
            qFatal("Error %s sending event %s to object %s (%s)",
                e.what(), typeid(*event).name(), qPrintable(receiver->objectName()),
                typeid(*receiver).name());
        } catch (...) {
            qFatal("Error <unknown> sending event %s to object %s (%s)",
                typeid(*event).name(), qPrintable(receiver->objectName()),
                typeid(*receiver).name());
        }

        // qFatal aborts, so this isn't really necessary
        // but you might continue if you use a different logging lib
    }

private:
    static void instanceLanguage() noexcept {
        auto&& lang = options::Language::currentBcp47Name();
        LOG_WARNING(tr()->load(lang, program::paths::langs()), "Can't load translation file '%1' from a directory %2", lang, program::paths::langs());
        LOG_WARNING(inherited::installTranslator(tr()), "Can't add translation file '%1' from a directory %2 to the Application", lang, program::paths::langs());
        QLocale::setDefault(options::Language::current().lang());
    }

public:
    explicit App(int& argc, char** argv) noexcept
        :inherited(argc, argv) {
        init();
        instanceLanguage();
    }
    static void init() noexcept {
        QCoreApplication::setOrganizationName(program::organizationName());
        QCoreApplication::setOrganizationDomain(program::organizationDomain());
        QCoreApplication::setApplicationName(program::name());
        QCoreApplication::setApplicationVersion(program::version());
    }
    static Settings* settings() noexcept {
        static Settings settings;
        return &settings;
    }
    static QTranslator* tr() noexcept {
        static QTranslator tr;
        return &tr;
    }
    static void exitWithError(QString const& error) noexcept {
        LOG_WARNING(false, "Exit from the program with an error", error);
        inherited::exit(1);
    }
    static void exitWithError(QString&& error) noexcept {
        LOG_WARNING(false, "Exit from the program with an error", error);
        inherited::exit(1);
    }
    static void exit() noexcept {
        inherited::exit();
    }
    static void quit() noexcept {
        inherited::instance()->quit();
    }
    QString const& exeName() noexcept {
        return inherited::arguments()[0];
    }
};
