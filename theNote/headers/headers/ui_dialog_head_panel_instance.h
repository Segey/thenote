/**
 * \file      c:/projects/thenote/theNote/headers/headers/instance/ui_dialog_head_panel_instance.h 
 * \brief     The Ui_dialog_head_panel_instance class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   October   (the) 15(th), 2019, 23:59 MSK
 * \updated   October   (the) 15(th), 2019, 23:59 MSK
 * \TODO      
**/
#pragma once
#include <QIcon>
#include <QLabel>
#include <QFrame>
#include <QObject>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <ps2/qt/instance4/panel.h>
#include "sources/font/font_instances.h"

/** \namespace ui */
namespace ui::instance {

class DialogHeadPanel: public ps2::instance4::Panel {
public:
    using class_name = DialogHeadPanel;
    using inherited  = ps2::instance4::Panel;

public:
    QLabel* m_pixmap  = Q_NULLPTR;
    QLabel* m_caption = Q_NULLPTR;
    QLabel* m_title   = Q_NULLPTR;

private:
    static inline QFont createCaptionFont(QFont const& font) noexcept {
        return FontInstance::bold(FontInstance::h2(font));
    }

private:
    void createIcon() noexcept {
        m_pixmap = new QLabel(parent);
        m_pixmap->setObjectName(QStringLiteral("head_icon"));
        m_pixmap->setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred));
    }
    QFrame* createLine() noexcept {
        auto line = new QFrame(parent);
        line->setObjectName(QStringLiteral("head_line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        return line;
    }
    void createCaption() noexcept {
        m_caption = new QLabel(parent);
        m_caption->setObjectName(QStringLiteral("head_caption"));
        m_caption->setFont(createCaptionFont(m_caption->font()));
    }
    void createTitle() noexcept {
        m_title = new QLabel(parent);
        m_title->setObjectName(QStringLiteral("head_title"));
        m_title->setAlignment(Qt::AlignBottom|Qt::AlignRight|Qt::AlignTrailing);
    }

private:
    virtual void doInstanceForm() noexcept Q_DECL_OVERRIDE {
        inherited::setObjectName(QStringLiteral("DialogHeadPanel"));
    }
    virtual void doInstanceWidgets() noexcept Q_DECL_OVERRIDE {
        createIcon();
        createCaption();
        createTitle();
    }
    virtual void doInstanceLayouts() noexcept Q_DECL_OVERRIDE {
        auto head = new QHBoxLayout;
        head->addWidget(m_pixmap);
        head->addWidget(m_caption);
        head->addWidget(m_title);

        auto lay = new QVBoxLayout(parent);
        lay->setMargin(0);
        lay->addLayout(head);
        lay->addWidget(createLine());
    }
    virtual void doTranslate() noexcept Q_DECL_OVERRIDE {}
};

} // end namespace ui::instance
