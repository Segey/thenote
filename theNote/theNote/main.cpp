#include "sources/app.h"
#include "thenote.h"

int main(int argc, char *argv[])
{
    App a(argc, argv);

    Manager w;
    w.show();
    a.setActiveWindow(&w);

    return a.exec();
}
