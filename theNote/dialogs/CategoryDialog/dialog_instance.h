/**
 * \file      theNote/dialogs/category/dialog_instance.h 
 * \brief     The Dialog_instance class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   October   (the) 15(th), 2019, 23:00 MSK
 * \updated   October   (the) 15(th), 2019, 23:00 MSK
 * \TODO      
**/
#pragma once
#include "sources/consts/pixmap64.h"
#include "sources/instance/dialog.h"
#include "headers/ui_dialog_head_panel.h"

/** \namespace dialog::category */
namespace dialog::category {

template <class UI, class T>
class DialogInstance final: public the::instance::Dialog<UI, T> {
public:
    using class_name = DialogInstance<UI, T>;
    using inherited  = the::instance::Dialog<UI, T>;

public:
    QString m_name;
    QString m_title_item;

private:
    virtual void doInstanceWidgets() noexcept Q_DECL_OVERRIDE Q_DECL_FINAL {
        inherited::createHead(pixmap64::category(), inherited::title()
                              , m_title_item);
        inherited::setOkButtonEnabled(inherited::ui->m_buttons, false);
        inherited::instanceForm(m_name);
    }

public:
    explicit DialogInstance(QString const& name, QString const& title_item)
        : m_name(name)
        , m_title_item(title_item) {
    }
};

} // end namespace dialog::category
