/**
 * \file      perfect/main/consts.h
 * \brief     Typedefs
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.5
 * \created   June     (the) 28(th), 2015, 16:10 MSK
 * \updated   February (the) 27(th), 2016, 13:48 MSK
 * \TODO
**/
#pragma once
#include <QIcon>
#include <QString>
#include <QPixmap>
//#include "consts/pixmap64.h"

namespace img8 {
    inline QString info() noexcept {
        return QStringLiteral(":/png8/info.png");
    }
}
namespace img12 {
    inline QString info() noexcept {
        return QStringLiteral(":/png12/info.png");
    }
}
namespace img16 {
    inline QString star() noexcept {
        return QStringLiteral(":/png16/star.png");
    }
    inline QString heart_rate() noexcept {
        return QStringLiteral(":/png16/heart-rate.png");
    }
}
namespace ico {
    static inline QIcon program() noexcept {
        return QIcon(QStringLiteral(":/ico/irondoom.ico"));
    }
}
namespace ico12 {
    static inline QIcon info() noexcept {
        return QIcon(img12::info());
    }
}
namespace ico16 {
    inline QIcon add() noexcept {
        return QIcon(QStringLiteral(":/png16/add.png"));
    }
    inline QIcon edit() noexcept {
        return QIcon(QStringLiteral(":/png16/edit.png"));
    }
    inline QIcon thanks() noexcept {
        return QIcon(QStringLiteral(":/png16/ok.png"));
    }
    inline QIcon star() noexcept {
        return QIcon(img16::star());
    }
    inline QIcon moveUp() noexcept {
        return QIcon(QStringLiteral(":/png16/action_arrow_up.png"));
    }
    inline QIcon moveDown() noexcept {
        return QIcon(QStringLiteral(":/png16/action_arrow_down.png"));
    }
    inline QIcon print() noexcept {
        return QIcon(QStringLiteral(":/png16/print.png"));
    }
    inline static QIcon document() noexcept {
        return QIcon(QStringLiteral(":/png16/document.png"));
    }
    inline static QIcon document_add() noexcept {
        return QIcon(QStringLiteral(":/png16/document-add.png"));
    }
    inline static QIcon document_open() noexcept {
        return QIcon(QStringLiteral(":/png16/document-open.png"));
    }
    inline static QIcon document_edit() noexcept {
        return QIcon(QStringLiteral(":/png16/document-edit.png"));
    }
    inline static QIcon document_remove() noexcept {
        return QIcon(QStringLiteral(":/png16/document-remove.png"));
    }
    inline static QIcon documents() noexcept {
        return QIcon(QStringLiteral(":/png16/documents.png"));
    }
    inline static QIcon documents_edit() noexcept {
        return QIcon(QStringLiteral(":/png16/documents-edit.png"));
    }
    inline static QIcon documents_remove() noexcept {
        return QIcon(QStringLiteral(":/png16/documents-remove.png"));
    }
    inline static QIcon edit_undo() noexcept {
        return QIcon(QStringLiteral(":/png16/edit-undo.png"));
    }
    inline static QIcon edit_redo() noexcept {
        return QIcon(QStringLiteral(":/png16/edit-redo.png"));
    }
    inline static QIcon edit_cut() noexcept {
        return QIcon(QStringLiteral(":/png16/edit-cut.png"));
    }
    inline static QIcon edit_copy() noexcept {
        return QIcon(QStringLiteral(":/png16/edit-copy.png"));
    }
    inline static QIcon edit_paste() noexcept {
        return QIcon(QStringLiteral(":/png16/edit-paste.png"));
    }
    inline static QIcon show_window() noexcept {
        return QIcon(QStringLiteral(":/png16/show_window.png"));
    }
}
namespace ico32 {
    inline QIcon new_() noexcept {
        return QIcon(QStringLiteral(":/png32/new.png"));
    }
    inline QIcon newDb() noexcept {
        return QIcon(QStringLiteral(":/png32/new_database.png"));
    }
    inline QIcon openDb() noexcept {
        return QIcon(QStringLiteral(":/png32/open_database.png"));
    }
    inline QIcon saveDb() noexcept {
        return QIcon(QStringLiteral(":/png32/save_database.png"));
    }
    inline QIcon saveAsDb() noexcept {
        return QIcon(QStringLiteral(":/png32/save_as_database.png"));
    }
    inline QIcon exit() noexcept {
        return QIcon(QStringLiteral(":/png32/exit.png"));
    }
    inline QIcon help() noexcept {
        return QIcon(QStringLiteral(":/png32/help.png"));
    }
    inline QIcon about() noexcept {
        return QIcon(QStringLiteral(":/png32/about.png"));
    }
    inline QIcon cut() noexcept {
        return QIcon(QStringLiteral(":/png32/cut.png"));
    }
    inline QIcon copy() noexcept {
        return QIcon(QStringLiteral(":/png32/copy.png"));
    }
    inline QIcon paste() noexcept {
        return QIcon(QStringLiteral(":/png32/paste.png"));
    }
    inline QIcon clear() noexcept {
        return QIcon(QStringLiteral(":/png32/clear.png"));
    }
    inline QIcon redo() noexcept {
        return QIcon(QStringLiteral(":/png32/redo.png"));
    }
    inline QIcon undo() noexcept {
        return QIcon(QStringLiteral(":/png32/undo.png"));
    }
}
namespace ico64 {
    inline QIcon user() noexcept {
        return QIcon(QStringLiteral(":/png64/user.png"));
    }
    inline QIcon plus() noexcept {
        return QIcon(QStringLiteral(":/png64/user.png"));
    }
}
namespace pixmap {
    inline QPixmap aboutBackground() noexcept {
        return QPixmap(QStringLiteral(":/introduction/about_background.png"));
    }
    inline QPixmap aboutIco() noexcept {
        return QPixmap(QStringLiteral(":/introduction/about_icon.png"));
    }
    inline QPixmap aboutText() noexcept {
        return QPixmap(QStringLiteral(":/introduction/about_text.png"));
    }
    inline QPixmap top() noexcept {
        return QPixmap(QStringLiteral(":/introduction/top.png"));
    }
    inline QPixmap bottom() noexcept {
        return QPixmap(QStringLiteral(":/introduction/bottom.png"));
    }
}
namespace pixmap12 {
    inline QPixmap info() noexcept {
        return QPixmap(img12::info());
    }
}
namespace pixmap_panels {
    inline QPixmap measurement_history() noexcept {
        return QPixmap(QStringLiteral(":/panel/measurement_history.png"));
    }
    inline QPixmap options() noexcept {
        return QPixmap(QStringLiteral(":/panel/options.png"));
    }
    inline QPixmap personal_info() noexcept {
        return QPixmap(QStringLiteral(":/panel/personal_info.png"));
    }
    inline QPixmap photogallery() noexcept {
        return QPixmap(QStringLiteral(":/panel/photogallery.png"));
    }
    inline QPixmap personal_records() noexcept {
        return QPixmap(QStringLiteral(":/panel/record_history.png"));
    }
};
