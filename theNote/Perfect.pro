TEMPLATE = subdirs

SUBDIRS         += \
   ../limereport  \
    Libs           \
    Headers        \
    Widgets        \
    Dialogs        \
    Windows        \
    Docks          \
    Panels         \
    Gismos         \
    Reports        \
    Box            \
    Downloader     \
    Updater        \
    Foods          \
    Standarts      \
    Plugins        \
    Ld             \
    Records        \
    Unit5          \
    ExercisesGuide \
    BluePrint      \
    main           \
    Tools          \

CONFIG += ordered
