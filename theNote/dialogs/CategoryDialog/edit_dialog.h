/**
 * \file      theNote/dialogs/category/edit_dialog.h 
 * \brief     The Edit_dialog class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   October   (the) 15(th), 2019, 22:49 MSK
 * \updated   October   (the) 15(th), 2019, 22:49 MSK
 * \TODO      
**/
#pragma once
#include <QVariant>
#include "dialog.h"

/** \namespace dialog::category */
namespace dialog::category {

class CATEGORY_DIALOG_SHARED_EXPORT EditDialog final: public Dialog {
public:
    using class_name = EditDialog;
    using inherited  = Dialog;

private:
    QString m_category;

private:
    virtual bool doIsChanged() const override final;
    virtual bool doIsValid() const override final;

public:
    explicit EditDialog(QString const& title
        , QString const& title_item, QString const& category_title
        , QWidget* parent = nullptr);
    explicit EditDialog(QString const& category, QString const& title
        , QString const& title_item, QString const& category_title
        , QWidget* parent);
    virtual ~EditDialog() = default;
    void setCategory(QString const& category) noexcept;
};

} // end namespace dialog::category
