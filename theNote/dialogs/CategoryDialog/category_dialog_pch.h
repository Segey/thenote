/**
 * \file      theNote/dialogs/category/category_dialog_pch.h 
 * \brief     The Category_dialog_pch class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   October   (the) 15(th), 2019, 22:58 MSK
 * \updated   October   (the) 15(th), 2019, 22:58 MSK
 * \TODO      
**/
#include <set>
#include <memory>
#include <QString>
#include <QVector>
#include <QVariant>
#include <QCheckBox>
#include <QtCore/qglobal.h>
