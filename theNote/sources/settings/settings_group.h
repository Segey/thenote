/**
 * \file      c:/projects/thenote/theNote/sources/settings/settings_group.h 
 * \brief     The Settings_group class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   October   (the) 18(th), 2019, 00:05 MSK
 * \updated   October   (the) 18(th), 2019, 00:05 MSK
 * \TODO      
**/
#pragma once
#include "settings.h"

/**
 * \code
    void settingsRead() {
        SettingsGroup settings(settings_pathes::panel(name()));
        m_first_day_of_week = settings.value(QStringLiteral("firstDayOfWeek")).toUInt();
        m_splash_screen = settings.value(QStringLiteral("splashScreen")).toBool();
    }
    void settingsWrite() {
        SettingsGroup settings(QStringLiteral("/options/"));
        settings.setValue(QStringLiteral("firstDayOfWeek"), m_first_day_of_week);
        settings.setValue(QStringLiteral("splashScreen"), m_splash_screen);
    }
 * \endcode
**/
class SettingsGroup: public Settings {
public:
    using class_name = SettingsGroup;
    using inherited  = Settings;
    using settings_t = QSettings;

public:
    SettingsGroup(QString const& prefix) noexcept {
        inherited::beginGroup(prefix);
    }
    SettingsGroup(QString&& prefix) noexcept {
        inherited::beginGroup(qMove(prefix));
    }
    virtual ~SettingsGroup() noexcept {
        inherited::endGroup();
    }
 };
