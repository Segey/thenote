#include "edit_dialog.h"
#include "ui_category_dialog.h"

/** \namespace dialog::category */
namespace dialog::category {

EditDialog::EditDialog(QString const& title
        , QString const& title_item, QString const& category_title
        , QWidget* parent)
    : EditDialog(QString(), title, title_item, category_title, parent)
{
}
EditDialog::EditDialog(QString const& category, QString const& title
        , QString const& title_item, QString const& category_title
        , QWidget* parent)
    : inherited(title, title_item, category_title, parent)
{
    setCategory(category);
}
bool EditDialog::doIsChanged() const
{
    return !((*ui)->m_category->text() == m_category);
}
bool EditDialog::doIsValid() const
{
    return inherited::isValid()
        && doIsChanged()
    ;
}
void EditDialog::setCategory(QString const& category) noexcept
{
    m_category = category;
    (*ui)->m_category->setText(category);
    inherited::doDisable((*ui)->m_buttons);
}

} // end namespace dialog::category
