#-------------------------------------------------
#
# Project created by QtCreator 2018-07-11T01:16:28
#
#-------------------------------------------------
TEMPLATE           = app
QT                += widgets sql core printsupport gui
TARGET             = theNote
PRECOMPILED_HEADER = theNote_pch.h

include(../theNote.pri)
MY_LIBS = -lcategorydlg

CONFIG(debug, debug|release) {
} else {
    CONFIG  -= console
}
LIBS += -L$$DESTDIR $$MY_LIBS

SOURCES +=      \
    main.cpp    \
    thenote.cpp \
    $$SOURCES_PATH/sources/tree/tree_model.cpp

HEADERS     += \
    thenote.h \
    $$SOURCES_PATH/sources/tree/tree_model.h
    $$SOURCES_PATH/sources/app.h

FORMS += \
        thenote.ui
