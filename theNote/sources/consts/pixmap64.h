/**
 * \file      c:/projects/thenote/theNote/sources/consts/pixmap64.h 
 * \brief     The Pixmap64 class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   October   (the) 18(th), 2019, 00:22 MSK
 * \updated   October   (the) 18(th), 2019, 00:22 MSK
 * \TODO      
**/
#pragma once
#include <QIcon>
#include <QString>
#include <QPixmap>

namespace pixmap64 {
    inline static QPixmap category() noexcept {
        return QPixmap(QStringLiteral(":/png64/document.png"));
    }
    //inline static QPixmap clause() noexcept {
        //return QPixmap(QStringLiteral(":/png64/clause.png"));
    //}
    //inline static QPixmap key() noexcept {
        //return QPixmap(QStringLiteral(":/png64/key.png"));
    //}
    //inline static QPixmap key_value() noexcept {
        //return QPixmap(QStringLiteral(":/png64/key-value.png"));
    //}
    //inline static QPixmap combobox() noexcept {
        //return QPixmap(QStringLiteral(":/png64/combobox.png"));
    //}
    //inline static QPixmap text_link() noexcept {
        //return QPixmap(QStringLiteral(":/png64/text-link.png"));
    //}
    //inline static QPixmap note() noexcept {
        //return QPixmap(QStringLiteral(":/png64/note.png"));
    //}
    //inline static QPixmap image() noexcept {
        //return QPixmap(QStringLiteral(":/png64/image.png"));
    //}
    //inline static QPixmap simple_records() noexcept {
        //return QPixmap(QStringLiteral(":/png64/simple-records.png"));
    //}
    //inline static QPixmap personal_records() noexcept {
        //return QPixmap(QStringLiteral(":/png64/personal-records.png"));
    //}
    //inline static QPixmap ms_body() noexcept {
        //return QPixmap(QStringLiteral(":/png64/blood-pressure-records.png"));
    //}
    //inline static QPixmap ms_blood() noexcept {
        //return QPixmap(QStringLiteral(":/png64/blood-pressure-records.png"));
    //}
    //inline static QPixmap diary_report() noexcept {
        //return QPixmap(QStringLiteral(":/png64/diary-report.png"));
    //}
    //inline static QPixmap esc() noexcept {
        //return QPixmap(QStringLiteral(":/png64/expr_set.png"));
    //}
    //inline static QPixmap expr_practice() noexcept {
        //return QPixmap(QStringLiteral(":/png64/expr_practice.png"));
    //}
    //inline static QPixmap result_report() noexcept {
        //return QPixmap(QStringLiteral(":/png64/result-report.png"));
    //}
    //inline static QPixmap plus() noexcept {
        //return QPixmap(QStringLiteral(":/png64/user.png"));
    //}
    //inline QPixmap user() noexcept {
        //return QPixmap(QStringLiteral(":/png64/user.png"));
    //}
    //inline QPixmap photos() noexcept {
        //return QPixmap(QStringLiteral(":/png64/photos.png"));
    //}
    //inline QPixmap measurement_history() noexcept {
        //return QPixmap(QStringLiteral(":/png64/measurement_history.png"));
    //}
    //inline QPixmap measurement_history_dialog() noexcept {
        //return QPixmap(QStringLiteral(":/png64/measurement_history_dialog.png"));
    //}
    //inline QPixmap measurement_history_method() noexcept {
        //return QPixmap(QStringLiteral(":/png64/measurement_history_method.png"));
    //}
    //inline QPixmap options() noexcept {
        //return QPixmap(QStringLiteral(":/png64/photos.png"));
    //}
    //inline QPixmap information() noexcept {
        //return QPixmap(QStringLiteral(":/png64/information.png"));
    //}
    //inline QPixmap exercise_speed_execution() noexcept {
        //return QPixmap(QStringLiteral(":/png64/exercise-speed-execution.png"));
    //}
    //inline QPixmap lifting_level() noexcept {
        //return QPixmap(QStringLiteral(":/png64/lifting-level.png"));
    //}
    //inline QPixmap exercise_type() noexcept {
        //return QPixmap(QStringLiteral(":/png64/exercise-type.png"));
    //}
    //inline QPixmap exercise_sport() noexcept {
        //return QPixmap(QStringLiteral(":/png64/exercise-sport.png"));
    //}
    //inline QPixmap exercise_force() noexcept {
        //return QPixmap(QStringLiteral(":/png64/exercise-force.png"));
    //}
    //inline QPixmap exercise_age() noexcept {
        //return QPixmap(QStringLiteral(":/png64/exercise-age.png"));
    //}
    //inline QPixmap exercise_adaptation() noexcept {
        //return QPixmap(QStringLiteral(":/png64/exercise-adaptation.png"));
    //}
    //inline QPixmap exercise_mechanics() noexcept {
        //return QPixmap(QStringLiteral(":/png64/exercise-mechanics.png"));
    //}
    //inline QPixmap exercise_equipment() noexcept {
        //return QPixmap(QStringLiteral(":/png64/exercise-equipment.png"));
    //}
    //inline QPixmap muscles() noexcept {
        //return QPixmap(QStringLiteral(":/png64/muscles.png"));
    //}
    //static inline QPixmap body_measurements_full() noexcept {
        //return QPixmap(QStringLiteral(":/png64/muscles.png"));
    //}
}

