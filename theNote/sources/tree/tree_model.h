/**
 * \file      c:/projects/thenote/theNote/main/tree/tree.h 
 * \brief     The Tree class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2018 
 * \version   v.1.0
 * \created   July      (the) 31(th), 2018, 00:00 MSK
 * \updated   July      (the) 31(th), 2018, 00:00 MSK
 * \TODO      
**/
#pragma once
#include <QVector>
#include <QVariant>
#include <QModelIndex>
#include <QAbstractItemModel>
#include <ps2/qt/array/field.h>
#include <ps2/qt/models/tree/simple_tree_item.h>

class TreeModel final: public QAbstractItemModel {
    Q_OBJECT

public:
    using class_name = TreeModel;
    using inherited  = QAbstractItemModel;
    using item_t     = ps2::SimpleTreeItem<QString>;
    using data_t     = ps2::Field<QString>;
    using ppair_t    = std::pair<item_t*, item_t*>;
    using ipair_t    = std::pair<QModelIndex, QModelIndex>;

private:
    item_t* m_root = nullptr;
    data_t m_data;

private:
    item_t* cast(QModelIndex const& index) const noexcept {
        if(index.isValid()) {
            auto item =  static_cast<item_t*>(index.internalPointer());
            if (item)
                return item;
        }
        return m_root;
    }

public:
     enum Roles {
          TypeRole     = Qt::UserRole + 1
        , PracticeRole = Qt::UserRole + 2
     };


protected:
    virtual QHash<int, QByteArray> roleNames() const noexcept override final {
         return {
               {TypeRole, "TypeRole"}
             , {PracticeRole, "PracticeRole"}
         };
    }

public:
    explicit TreeModel(QObject* parent = nullptr);
    virtual ~TreeModel() override;
    void init();
    QVariant data(QModelIndex const& index, int role) const override;
    Qt::ItemFlags flags(QModelIndex const& index) const override;
    QModelIndex index(int row, int column, QModelIndex const& parent = {}) const override;
    QModelIndex parent(QModelIndex const& index) const override;
    bool setData(QModelIndex const& index, QVariant const& value, int role) override final;
    int rowCount(QModelIndex const& parent = {}) const override final;
    int columnCount(QModelIndex const& parent = {}) const override;
    void addItem(QString const& data, QModelIndex const& index = {}) noexcept;
    //void editPractice(QModelIndex const& model, ExprPracticeCollection const& practice) noexcept;
    //void addSets(QModelIndex const& model, ExprSimpleCollection const& set, uint count) noexcept;
};
