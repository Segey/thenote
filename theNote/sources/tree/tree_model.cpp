#include <QStringList>
#include "tree_model.h"
#include "sources/tr.h"
#include "sources/log.h"
//#include <main/display/practice.h>

TreeModel::TreeModel(QObject* parent)
    : QAbstractItemModel(parent)
    , m_root(new item_t)
{
}
TreeModel::~TreeModel()
{
    delete m_root;
}
int TreeModel::columnCount(QModelIndex const& parent) const
{
    Q_UNUSED(parent)
    return 1;
}
QVariant TreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return {};

    if(role == Qt::DisplayRole) {
        auto item = cast(index);
        return item->data();
    }


    //auto const& p = indexes(index).first;
    //auto const practice = m_data[p.row()].value;

    //if(role == TypeRole)
     //   return ps2::to_variant(practice.set().type());
    //if(role == PracticeRole)
     //   return ps2::to_variant(practice);
    return {};
}
Qt::ItemFlags TreeModel::flags(QModelIndex const& index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
}
QModelIndex TreeModel::index(int row, int column, QModelIndex const& parent) const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    auto parentItem = m_root;

    if (parent.isValid())
        parentItem = cast(parent);

    auto childItem = parentItem->child(row);
    return !childItem ?
        QModelIndex() : createIndex(row, column, childItem);
}
QModelIndex TreeModel::parent(QModelIndex const& index) const
{
    if (!index.isValid())
        return QModelIndex();

    auto childItem = cast(index);
    LOG_CRITICAL(childItem != nullptr, "Child item cannot be null");
    auto parentItem = childItem->parent();

    if (parentItem == m_root)
        return QModelIndex();

    return inherited::createIndex(parentItem->row(), 0, parentItem);
}
bool TreeModel::setData(QModelIndex const& index, QVariant const& value, int role)
{
    if (role != Qt::EditRole)
        return false;

    auto item = cast(index);
    item->setData(value.toString());

    //if (result)
     //   emit dataChanged(index, index, {Qt::DisplayRole, Qt::EditRole});

    return true;
}
int TreeModel::rowCount(QModelIndex const& parent) const
{
    auto parentItem = cast(parent);
    return parentItem ? parentItem->childCount() : 0;
}
void TreeModel::addItem(QString const& data, QModelIndex const& index) noexcept
{
    auto const row = rowCount();
    inherited::beginInsertRows({}, row, row + 1);
    auto parent = cast(index);
    auto parentItem = new item_t(parent, data);
    parent->addChild(parentItem);
    inherited::endInsertRows();
}
//void TreeModel::editPractice(QModelIndex const& index
                             //, ExprPracticeCollection const& practice) noexcept
//{
    //if(!index.isValid() || practice.set().setsSize() == 0)
        //return;

    //auto const& pts = points(index);
    //auto const& ids = indexes(index);
    //auto& d = m_data[ids.first.row()];
    //d.value = practice;

    //pts.first->setData({{practice.exercise().name()}});

    //item_t::data_t list;
    //list << ps2::to_qstr(practice.set().type());
    //for(auto const& p : practice.set().sets())
        //list << display::showExprSet(p);

    //pts.second->setData(list);
//}
//void TreeModel::addSets(QModelIndex const& index, ExprSimpleCollection const& set, uint count) noexcept
//{
    //if(!index.isValid() || m_data.size() < index.row() || set.isSetsEmpty())
        //return;

    //auto const& pts = points(index);
    //auto const& ids = indexes(index);
    //auto& d = m_data[ids.first.row()];

    //auto const& data = display::showExprSet(set.at(0));
    //auto& cols = d.value.set().sets();
    //for(decltype(count) i = 0; i != count; ++i) {
        //pts.second->addColumnData(data);
        //cols.push_back(set.at(0));
    //}
    //m_data.edit(ids.first.row(), d.value);
//}
void TreeModel::init()
{
    //addItem("Cool");
    //addCategory("Cool");
    //addCategory("Cool 3");
    //addItem("Cool 1-0", class_name::index(0, 0));
    //addItem("Cool 2-0", class_name::index(0, 0, class_name::index(0,0)));
    //addItem(class_name::index(2, 1), "Cool 2");
    //addItem(class_name::index(2, 0), "Cool 444");
}
