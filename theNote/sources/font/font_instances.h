/**
 * \file      c:/projects/thenote/theNote/sources/font/font_instances.h 
 * \brief     The Font_instances class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   October   (the) 17(th), 2019, 23:48 MSK
 * \updated   October   (the) 17(th), 2019, 23:48 MSK
 * \TODO      
**/
#pragma once
#include <QFont>

class FontInstance final {
public:
    using class_name = FontInstance;
     
private: 
    static inline QFont h(QFont const& font, int num) noexcept {
        auto f = font;
        f.setPointSize(f.pointSize() + num);
        return f;
    }

public:
    explicit FontInstance() Q_DECL_EQ_DEFAULT;
    ~FontInstance() Q_DECL_EQ_DEFAULT;
    static inline QFont h0(QFont const& font) noexcept {
        return h(font, 20);
    }
    static inline QFont h1(QFont const& font) noexcept {
        return h(font, 17);
    }
    static inline QFont h2(QFont const& font) noexcept {
        return h(font, 14);
    }
    static inline QFont h3(QFont const& font) noexcept {
        return h(font, 10);
    }
    static inline QFont h4(QFont const& font) noexcept {
        return h(font, 7);
    }
    static inline QFont h5(QFont const& font) noexcept {
        return h(font, 4);
    }
    static inline QFont h6(QFont const& font) noexcept {
        return h(font, 1);
    }
    static inline QFont h7(QFont const& font) noexcept {
        return h(font, -1);
    }
    static inline QFont h8(QFont const& font) noexcept {
        return h(font, -2);
    }
    /**
     * \code
     *     widget->setFont(FontInstance::bold(label->font()));
     * \endcode
    **/
    static inline QFont bold(QFont const& font) noexcept {
        auto f = font;
        f.setBold(true);
        return f;
    }
    /**
     * \code
     *     widget->setFont(FontInstance::italic(label->font()));
     * \endcode
    **/
    static inline QFont italic(QFont const& font) noexcept {
        auto f = font;
        f.setItalic(true);
        return f;
    }
    /**
     * \code
     *     widget->setFont(FontInstance::underline(label->font()));
     * \endcode
    **/
    static inline QFont underline(QFont const& font) noexcept {
        auto f = font;
        f.setUnderline(true);
        return f;
    }
};
