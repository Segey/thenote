#-------------------------------------------------
#
# Project created 2019-10-15T23:07:37
#
#-------------------------------------------------
THENOTE_PATH      = $$PWD
PS2_PATH          = $$PWD/../ps2
RESOURCES_PATH    = $$PWD
SOURCES_PATH      = $$PWD
TRANSLATIONS_PATH = $$PWD/resources/langs
OUTPUT_PATH       = $$PWD/../../bin/qmake

CONFIG             += c++1z console precompile_header
DEFINES            += QT \
                      QT_DEPRECATED_WARNINGS
QMAKE_CXXFLAGS     += -Wall
INCLUDEPATH        += .

CONFIG(debug, debug|release) {
    DEFINES += _DEBUG
    CONFIG  += console
    BUILD_TYPE = debug
} else {
    BUILD_TYPE = release
    CONFIG    += build_translations  # Updates ts files
}

unix{
    OS_TYPE = unix
    macx{
        OS_TYPE = macx
    }
    linux{
        QMAKE_CXXFLAGS -= -Wmismatched-tags
        !contains(QT_ARCH, x86_64){
            message("Compiling for 32bit system")
            OS_TYPE = linux32
        }else{
            message("Compiling for 64bit system")
            OS_TYPE = linux64
        }
    }
}
win32 {
    OS_PATH = win32
    QMAKE_CXXFLAGS += -wd4096 \
                      -wd4099 \
                      -wd4146 \
                      -wd4365 \
                      -wd4371 \
                      -wd4571 \
                      -wd4599 \
                      -wd4619 \
                      -wd4625 \
                      -wd4626 \
                      -wd4628 \
                      -wd4686 \
                      -wd4774 \
                      -wd4820 \
                      -wd4868 \
                      -wd4946 \
                      -wd4996 \
                      -wd5026 \
                      -wd5027

    QMAKE_CXXFLAGS += -std:c++17
}

DEPENDPATH +=              \
    $$PS2_PATH              \
    $$SOURCES_PATH          \
    $$THENOTE_PATH/headers/ \
    $$THENOTE_PATH/dialogs/ \

INCLUDEPATH +=              \
    $$PS2_PATH              \
    $$SOURCES_PATH          \
    $$THENOTE_PATH/headers/ \
    $$THENOTE_PATH/dialogs/ \

HEADERS     += \
    $$SOURCES_PATH/sources/tr.h

DESTDIR   = $${OUTPUT_PATH}/$${BUILD_TYPE}

RESOURCES += \
    $$RESOURCES_PATH/resources/images/default.qrc \
    $$RESOURCES_PATH/resources/images/ico.qrc \
    $$RESOURCES_PATH/resources/images/introduction.qrc \
    $$RESOURCES_PATH/resources/images/panel.qrc \
    $$RESOURCES_PATH/resources/images/png8.qrc \
    $$RESOURCES_PATH/resources/images/png12.qrc \
    $$RESOURCES_PATH/resources/images/png16.qrc \
    $$RESOURCES_PATH/resources/images/png32.qrc \
    $$RESOURCES_PATH/resources/images/png64.qrc \
    $$RESOURCES_PATH/resources/images/png48.qrc \

TRANSLATIONS += \
    $$TRANSLATIONS_PATH/en.ts \
    $$TRANSLATIONS_PATH/ru.ts

QMAKE_CXXFLAGS += -DQT_NO_CAST_FROM_ASCII      \
                  -DQT_NO_CAST_TO_ASCII        \
                  -DQT_NO_CAST_FROM_BYTEARRAY  \
                  -DQT_NO_URL_CAST_FROM_STRING \
                  -DQT_USE_QSTRINGBUILDER      \
                  -DD_SCL_SECURE_NO_WARNINGS

######## change console output to cp1251 #######
QMAKE_EXTRA_TARGETS += before_build makefilehook

makefilehook.target = $(MAKEFILE)
makefilehook.depends = .beforebuild

PRE_TARGETDEPS += .beforebuild


before_build.target = .beforebuild
before_build.depends = FORCE
before_build.commands = chcp 1251
