#include "ui_dialog_head_panel.h"
#include "ui_dialog_head_panel_instance.h"

/** \namespace ui */
namespace ui {

DialogHeadPanel::DialogHeadPanel(QWidget* parent, QPixmap const& pixmap, QString const& caption, QString const& title)
    : inherited(parent)
    , m_pixmap(pixmap)
    , m_caption(caption)
    , m_title(title)
    , ui(new instance_t)
{
}
DialogHeadPanel::DialogHeadPanel(QPixmap const& pixmap, QString const& caption, QString const& title)
    : class_name(Q_NULLPTR, pixmap, caption, title)
{
}
 DialogHeadPanel::~DialogHeadPanel()
{
    delete ui;
}
void DialogHeadPanel::instanceAfter()
{
    ui->m_caption->setText(m_caption);
    ui->m_pixmap->setPixmap(m_pixmap);
    ui->m_title->setText(m_title);
}
void DialogHeadPanel::instance(QWidget* parent)
{
    if (parent == Q_NULLPTR) parent = inherited::parentWidget();
    ui->instance(parent);
    instanceAfter();
}
QPixmap const& DialogHeadPanel::pixmap() const
{
    return m_pixmap;
}
void DialogHeadPanel::setIcon(QPixmap const& pixmap) noexcept
{
    m_pixmap = pixmap;
}
QString const& DialogHeadPanel::title() const
{
    return m_title;
}
void DialogHeadPanel::setTitle(QString const& title)
{
    m_title = title;
}
QString const& DialogHeadPanel::caption() const
{
    return m_caption;
}
void DialogHeadPanel::setCaption(QString const& caption) noexcept
{
    m_caption = caption;
}
void DialogHeadPanel::setCaption(QString&& caption) noexcept
{
    m_caption = qMove(caption);
}

} // end namespace ui
