/**
 * \file      c:/projects/thenote/theNote/main/program.h 
 * \brief     The Program class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2018 
 * \version   v.1.0
 * \created   July      (the) 12(th), 2018, 00:35 MSK
 * \updated   July      (the) 12(th), 2018, 00:35 MSK
 * \TODO      
**/
#pragma once
#include <QDir>
#include <QDate>
#include <QSettings>
#include <QFileInfo>
#include <QStringList>
#include <QApplication>
#include <QStandardPaths>

#define APP_PATH(str) QCoreApplication::applicationDirPath() + QStringLiteral(str);
#define HOME_PATH(str) program::homeFolder() + QStringLiteral(str);
/**
 * \code
 *      program::fullName();
 * \endcode
**/
namespace program {
    inline QString name() noexcept {
        return QStringLiteral("the Note");
    }
    inline QString version() noexcept {
        return QStringLiteral("0.1");
    }
    inline QString fullName() noexcept {
        return QStringLiteral("%1 %2")
               .arg(name())
               .arg(version());
    }
    inline QString date() noexcept {
        return QDate(2018,07,12).toString(Qt::ISODate);
    }
    inline QString build() noexcept {
        return QStringLiteral("0001");
    }
    inline QString fullVersion() noexcept {
        return QStringLiteral("%1 %2, build %3 (%4)")
                .arg(name())
                .arg(version())
                .arg(build())
                .arg(date());
    }
    inline QString organizationName() noexcept {
        return QStringLiteral("FiveSoft");
    }
    inline QString organizationDomain() noexcept {
        return QStringLiteral("irondoom.ru");
    }
    inline QString homeFolder() noexcept {
        QSettings s(QSettings::IniFormat, QSettings::UserScope, QStringLiteral(".thenote"), QStringLiteral(""));
        return QFileInfo(s.fileName()).absolutePath() + QStringLiteral("/.thenote");
    }
    inline QString help() noexcept {
        return APP_PATH("/help/help.chm");
    }
    inline QString author() noexcept {
        return QStringLiteral("Sergey Panin");
    }
    inline QString copyright() noexcept {
        return QStringLiteral("Copyright (c) 2018, S.Panin. All rights reserved.");
    }
    inline QString homepage() noexcept {
        return QStringLiteral("http://www.irondoom.ru");
    }
    static inline QString logFile() noexcept {
        return HOME_PATH("/log.txt");
    }
    inline QString supportMail() noexcept {
        return QStringLiteral("dix75@mail.ru");
    }
    /** \namespace registry */
    namespace registry {
        static inline QString software() noexcept {
            return QStringLiteral("HKEY_CURRENT_USER\\SOFTWARE");
        }
        static inline QString theNote() noexcept {
            return QStringLiteral("HKEY_CURRENT_USER\\SOFTWARE\\FiveSoft\\theNot");
        }
    } // end namespace registry
    /**
     * \code
     *      program::web::new_version::info();
     * \endcode
    **/
    namespace ext {
        static inline QString baseDb() noexcept {
            return QStringLiteral("*.ldb *.ldb2 *.ldb3");
        }
    }
    namespace default_file {
        inline QString settingsDb() noexcept {
            return HOME_PATH("/settings.notedb");
        }
    }
    namespace script {
        inline QString settingsDb() noexcept {
            return QLatin1String("thenote.scr");
        }
    }
    namespace paths {
        static inline QString langs() noexcept {
            return APP_PATH("/langs");
        }
    }
}
