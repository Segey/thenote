/**
 * \file      thenote/theNote/sources/widgets/menu/menu_help.h 
 * \brief     The Menu_help class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   October   (the) 17(th), 2019, 23:55 MSK
 * \updated   October   (the) 17(th), 2019, 23:55 MSK
 * \TODO      
**/
#pragma once
#include <QUrl>
#include <QMenu>
#include <QAction>
#include <QString>
#include <QMessageBox>
#include <QDesktopServices>
#include "sources/action.h"
#include "sources/program.h"
//#include <AboutDialog/dialog.h>

class MenuHelp: public QMenu {
public:
    using inherited  = QMenu;
    using class_name = MenuHelp;

private:
    QAction* m_help = Q_NULLPTR;
    QAction* m_homepage = Q_NULLPTR;
    QAction* m_registration = Q_NULLPTR;
    QAction* m_about = Q_NULLPTR;

    void createActions() noexcept {
        m_help         = Action::createHelp(this);
        m_homepage     = Action::createHomePage(this);
        m_registration = Action::createRegistration(this);
        m_about        = Action::createAbout(this);
    }
    void instanceSignals() noexcept {
        connect(m_help, &QAction::triggered, [](){
            QDesktopServices::openUrl(QUrl(program::help()));
        });
        connect(m_homepage, &QAction::triggered, [](){
            QDesktopServices::openUrl(QUrl(program::homepage()));
        });
        connect(m_registration, &QAction::triggered, [](){
            /*_XXX_*/
        });
        connect(m_about, &QAction::triggered, []() {
            //AboutDialog dialog;
            //dialog.exec();
        });
    }
    void instanceActions() noexcept {
        inherited::addAction(m_help);
        inherited::addSeparator();
        inherited::addAction(m_homepage);
      //  inherited::addAction(m_registration);
        inherited::addSeparator();
        inherited::addAction(m_about);
    }
    void translate() noexcept {
        inherited::setTitle(iMenu::tr("&Help"));
    }

public:
    /**
     * \code
     *      void instanceHelpMenu() {
     *          m_parent->m_help = new MenuHelp(m_parent);
     *          m_parent->menuBar()->addMenu(m_parent->m_help);
     *      }
     * \endcode
    **/
    explicit MenuHelp(QWidget* parent) noexcept
        : inherited(parent)  {
        createActions();
        instanceActions();
        instanceSignals();
        translate();
    }
};
