/**
 * \file      c:/projects/thenote/theNote/thenote.h 
 * \brief     The Thenote class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2018 
 * \version   v.1.0
 * \created   July      (the) 30(th), 2018, 22:52 MSK
 * \updated   July      (the) 30(th), 2018, 22:52 MSK
 * \TODO      
**/
#pragma once
#include <memory>
#include <QMainWindow>
#include "sources/widgets/window/main_window.h"
#include "sources/tree/tree_model.h"

THE_UI(Manager)

class Manager : public QMainWindow, widget::MainWindow {
Q_OBJECT
Q_DISABLE_COPY(Manager)

template<typename T, typename U>
friend class ManagerInstance;

public:
    using class_name    = Manager;
    using inherited     = QMainWindow;
    using ui_t          = Ui::Manager;
    using main_window   = widget::MainWindow;
    //using table_model_t = Traits::table_model_t;
    using view_model_t  = TreeModel;
    //using item_t        = Traits::item_t;
    //using reader_t      = Traits::reader_t;
    //using writer_t      = Traits::writer_t;
    //using full_view_t   = Traits::full_view_t;

private:
    std::unique_ptr<ui_t> ui;
    view_model_t  m_view;
    //table_model_t m_table;

private:
    void instanceViewModel() noexcept;
    void instanceTableModel() noexcept;
    void instanceSignals() noexcept;
    void selectFirstRow() noexcept;
    bool isSelected() const noexcept;

private slots:
    void onAddCategory() noexcept;
    void onEditDay() noexcept;
    void onRemoveDay() noexcept;
    void onRemoveAllDays() noexcept;
    void onUpdate();
    void onSelect();
    void onAddItem() noexcept;
    void onRemoveItem() noexcept;
    void onEditItem() noexcept;
    void onViewModelClicked(QModelIndex const& index) noexcept;

public:
    explicit Manager(QWidget* parent = nullptr) noexcept;
    virtual ~Manager() override;
};
