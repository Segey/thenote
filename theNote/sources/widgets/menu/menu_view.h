/**
 * \file      c:/projects/thenote/theNote/sources/widgets/menu/menu_view.h 
 * \brief     The Menu_view class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   October   (the) 17(th), 2019, 23:54 MSK
 * \updated   October   (the) 17(th), 2019, 23:54 MSK
 * \TODO      
**/
#pragma once
#include <initializer_list>
#include <QMenu>
#include <QVector>
#include <QDockWidget>
#include <main/log.h>
#include <main/action.h>

class MenuView : public QMenu {
    Q_OBJECT

public:
    using class_name = MenuView;
    using inherited  = QMenu;
    using dock_t     = QDockWidget;

private:
    QVector<dock_t*> m_vec;

private:
    QAction* createEmptyAction() noexcept {
        auto result = new QAction(this);
        result->setCheckable(true);
        return result;
    }
    void createActions() noexcept {
        for(auto i = 0; i != m_vec.size() ; ++i)
            QMenu::addAction(createEmptyAction());
    }
    void instanceDockSignals() noexcept {
        for(auto i = 0; i != m_vec.size() ; ++i) {
            connect(m_vec[i], &QDockWidget::visibilityChanged, [this, i](bool visible){
                inherited::actions().at(i)->setChecked(visible);
            });
        }
    }
    void instanceActionSignals() noexcept {
        for(auto i = 0; i != m_vec.size() ; ++i) {
            connect(inherited::actions().at(i), &QAction::triggered, [this, i](bool visible){
                m_vec[i]->setVisible(visible);
            });
        }
    }
    void translate() noexcept {
        inherited::setTitle(iMenu::tr("&View"));
    }
    void init() noexcept {
        createActions();
        instanceDockSignals();
        instanceActionSignals();
        translate();
    }

public:
    /**
      \code
            m_parent->m_menu_view = new MenuView(m_window,
                {m_parent->m_weight, m_parent->m_reps, m_parent->m_calculate});
            m_window->menuBar()->addMenu(m_parent->m_menu_view);
        \endcode
    **/
    explicit MenuView() noexcept;
    explicit MenuView(QWidget* parent) noexcept
        : inherited(parent) {
    }
    explicit MenuView(QWidget* parent, std::initializer_list<dock_t*> list) noexcept
        : inherited(parent)
        , m_vec(list) {
        init();
    }
    void init(std::initializer_list<dock_t*> list) noexcept {
        m_vec = list;
        init();
    }
    void setText(int index, QString const& str) {
        LOG_ERROR(index < QMenu::actions().size(),  "Out of range!!!");
        QMenu::actions()[index]->setText(str);
    }
    void setText(int index, QString&& str) {
        LOG_ERROR(index < QMenu::actions().size(), "Out of range!!!");
        QMenu::actions()[index]->setText(qMove(str));
    }
};
