/**
 * \file      c:/projects/thenote/theNote/main/log.h 
 * \brief     The Log class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2018 
 * \version   v.1.0
 * \created   July      (the) 21(th), 2018, 23:32 MSK
 * \updated   July      (the) 21(th), 2018, 23:32 MSK
 * \TODO      
**/
#pragma once
#include <QDebug>
#include <QSqlQuery>
#include <QJsonParseError>
#include <ps2/qt/log/program/program_log.h>
#include "sources/program.h"

extern bool g_log_maybe_exception;

/**
 * \code
 *     LOG_CRITICAL(true,
 * \endcode
**/
class Log final {
public:
    using class_name = Log;

private:
    uint m_type = 0u;
    std::string m_file;
    std::string m_fun;
    int m_line = 0;

private:
    template<class T>
    static inline QString toStr(T const& val) noexcept {
        QString s;
        QDebug(&s) << val;
        return s;
    }
    static inline QString toStr(int val) noexcept {
        return QLocale().toString(val);
    }
    static inline QString toStr(QSqlError const& err) noexcept {
        return err.text();
    }
    static inline QString toStr(QFile::FileError err) noexcept {
        QString s;
        if(QFile::NoError == err) s = QStringLiteral("NoError");
        else if(QFile::ReadError == err) s = QStringLiteral("ReadError");
        else if(QFile::WriteError == err) s = QStringLiteral("WriteError");
        else if(QFile::FatalError == err) s = QStringLiteral("FatalError");
        else if(QFile::ResourceError == err) s = QStringLiteral("ResourceError");
        else if(QFile::OpenError == err) s = QStringLiteral("OpenError");
        else if(QFile::AbortError == err) s = QStringLiteral("AbortError");
        else if(QFile::TimeOutError == err) s = QStringLiteral("TimeOutError");
        else if(QFile::UnspecifiedError == err) s = QStringLiteral("UnspecifiedError");
        else if(QFile::RemoveError == err) s = QStringLiteral("RemoveError");
        else if(QFile::RenameError == err) s = QStringLiteral("RenameError");
        else if(QFile::PositionError == err) s = QStringLiteral("PositionError");
        else if(QFile::ResizeError == err) s = QStringLiteral("ResizeError");
        else if(QFile::PermissionsError == err) s = QStringLiteral("PermissionsError");
        else if(QFile::CopyError == err) s = QStringLiteral("CopyError");
        else s = QStringLiteral("UnknownError FileError");

        return QStringLiteral("%1[%2]").arg(s, QLocale().toString(err));
    }
    static inline QString toStr(QJsonParseError const& err) noexcept {
        QString s;
        if(QJsonParseError::NoError == err.error) s = QStringLiteral("NoError");
        else if(QJsonParseError::UnterminatedObject == err.error) s = QStringLiteral("UnterminatedObject");
        else if(QJsonParseError::MissingNameSeparator == err.error) s = QStringLiteral("MissingNameSeparator");
        else if(QJsonParseError::UnterminatedArray == err.error) s = QStringLiteral("UnterminatedArray");
        else if(QJsonParseError::MissingValueSeparator == err.error) s = QStringLiteral("MissingValueSeparator");
        else if(QJsonParseError::IllegalValue == err.error) s = QStringLiteral("IllegalValue");
        else if(QJsonParseError::TerminationByNumber == err.error) s = QStringLiteral("TerminationByNumber");
        else if(QJsonParseError::IllegalNumber == err.error) s = QStringLiteral("IllegalNumber");
        else if(QJsonParseError::IllegalEscapeSequence == err.error) s = QStringLiteral("IllegalEscapeSequence");
        else if(QJsonParseError::IllegalUTF8String == err.error) s = QStringLiteral("IllegalUTF8String");
        else if(QJsonParseError::UnterminatedString == err.error) s = QStringLiteral("UnterminatedString");
        else if(QJsonParseError::MissingObject == err.error) s = QStringLiteral("MissingObject");
        else if(QJsonParseError::DeepNesting == err.error) s = QStringLiteral("DeepNesting");
        else if(QJsonParseError::DocumentTooLarge == err.error) s = QStringLiteral("DocumentTooLarge");
        else if(QJsonParseError::GarbageAtEnd == err.error) s = QStringLiteral("GarbageAtEnd");
        else s = QStringLiteral("UnknownError JsonParseError");

        return QStringLiteral("%1[%2:%3]").arg(err.errorString(), QLocale().toString(err.error), s);
    }
    static inline QString toStr(QString const& str) noexcept {
        return str;
    }
    static inline QString toStr(QSqlQuery const& query) noexcept {
        Q_UNUSED(query);
        return QString();
    }
    QString typedStr() const noexcept {
        QString s;
        if(m_type == 1)
            s = QStringLiteral("DEBUG");
        else if(m_type == 2)
            s = QStringLiteral("INFO");
        else if(m_type == 3)
            s = QStringLiteral("WARNING");
        else if(m_type == 4)
            s = QStringLiteral("CRITICAL");
        else if(m_type == 5)
            s = QStringLiteral("FATAL");
        return QStringLiteral("%1: ").arg(s);
    }
    inline void print(QString&& s) const noexcept {
#if defined _TEST && _DEBUG
        if(!g_log_maybe_exception)
            qDebug() << m_fun.c_str() << ":" << m_line << s << ", file" << m_file.c_str();
#elif _DEBUG
        if(m_type < 4)
            qDebug() << m_fun.c_str() << ":" << m_line << s << ", file" << m_file.c_str();
        else qt_assert_x(m_fun.c_str(), s.toLocal8Bit().constData(), m_file.c_str(),m_line);
#else
        ProgramFileLog(program::logFile(), m_fun.c_str(), m_file.c_str(), m_line).write(qMove(s));
#endif
    }

public:
    Log(uint type, std::string&& file, std::string&& fun, int line) noexcept
        : m_type(type)
        , m_file(qMove(file))
        , m_fun(fun)
        , m_line(line) {
    }
    void operator()(std::string&& s) noexcept {
        print(QString::fromStdString(s));
    }
    bool operator()(bool cond, std::string&& s) noexcept {
        if(cond)
            return true;
        print(QString::fromStdString(s));
        return false;
    }
    template<class T>
    bool operator()(bool cond, std::string&& text, T&& val) noexcept {
        if(cond)
            return true;

        auto&& args = QString::fromStdString(text).arg(toStr(val));
        auto&& s = QStringLiteral("%1%2").arg(typedStr(), qMove(args));
        print(qMove(s));
        return false;
    }
    template<class T1, class T2>
    bool operator()(bool cond, std::string&& text, T1&& val1, T2&& val2) noexcept {
        if(cond)
            return true;

        auto&& args = QString::fromStdString(qMove(text))
                .arg(toStr(std::forward<T1>(val1))
                   , toStr(std::forward<T2>(val2)));

        auto&& s = QStringLiteral("%1%2").arg(typedStr(), qMove(args));
        print(qMove(s));
        return false;
    }
    template<class T1, class T2, class T3>
    bool operator()(bool cond, std::string&& text, T1&& val1, T2&& val2, T3&& val3) noexcept {
        if(cond)
            return true;

        auto&& args = QString::fromStdString(qMove(text))
                .arg(toStr(std::forward<T1>(val1))
                   , toStr(std::forward<T2>(val2))
                   , toStr(std::forward<T3>(val3)));

        auto&& s = QStringLiteral("%1%2").arg(typedStr(), qMove(args));
        print(qMove(s));
        return false;
    }
};

#define LOG_DEBUG Log(1, __FILE__, __FUNCTION__, __LINE__)
#define LOG_INFO Log(2, __FILE__, __FUNCTION__, __LINE__)
#define LOG_WARNING Log(3, __FILE__, __FUNCTION__, __LINE__)
#define LOG_CRITICAL Log(4, __FILE__, __FUNCTION__, __LINE__)
#define LOG_FATAL Log(5, __FILE__, __FUNCTION__, __LINE__)

/**
 * \code
 *      auto const result = query::exec(query);
 *      if(!result || !query.next()) return 0;
 *
 *      auto const result = query::exec(query);
 *      return result ? query.lastInsertId().toUInt() : 0u;
 *
 *      auto const result = query::exec(query);
 *      return result ? toUint(query, 0) + 1u : 1001u;
 * \endcode
**/
/** \namespace query */
namespace query {

inline bool exec(QSqlQuery& query) {
    auto const result = query.exec();
    if(query.lastError().isValid()) {
        auto const& map = query.boundValues();
        QString values;
        foreach(QString const& s, map.keys())
            values += QStringLiteral(" %1=%2").arg(s)
                    .arg(map.value(s).toString());
        LOG_CRITICAL(qPrintable(query.lastQuery()+ query.lastError()
                                .text() + values));
    }
    return result;
}

} // end namespace query
