#-------------------------------------------------
#
# Project created by QtCreator 2019-17-10T00:00:23
#
#-------------------------------------------------
TEMPLATE            = lib
QT                 += widgets core
TARGET              = headers
DEFINES            += HEADERS_LIBRARY
PRECOMPILED_HEADER  = headers_pch.h

include(../theNote.pri)
LIBS += -L$$DESTDIR 

SOURCES += \
    headers/ui_dialog_head_panel.cpp

HEADERS += \
    headers_global.h \
    headers/ui_dialog_head_panel.h \
    headers/ui_dialog_head_panel_instance.h
