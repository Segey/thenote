/**
 * \file      c:/projects/thenote/theNote/sources/font/font_metrics.h 
 * \brief     The Font_metrics class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   October   (the) 17(th), 2019, 23:47 MSK
 * \updated   October   (the) 17(th), 2019, 23:47 MSK
 * \TODO      
**/
#pragma once
#include <QSize>
#include <QFont>
#include <QString>
#include <QFontMetrics>

class FontMetrics final {
public:
    using class_name = FontMetrics;
     
public:
    explicit FontMetrics() Q_DECL_EQ_DEFAULT;
    static inline int width(QFont const& font, QString const& text) {
        return QFontMetrics(font).width(text);
    }
    static inline int height(QFont const& font) {
        return QFontMetrics(font).height();
    }
    static inline QSize size(QFont const& font, QString const& text){
        return QSize(width(font, text), height(font));
    }
};

