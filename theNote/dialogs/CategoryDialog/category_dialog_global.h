/**
 * \file      theNote/dialogs/category/category_dialog_global.h 
 * \brief     The Category_dialog_global class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   October   (the) 15(th), 2019, 22:57 MSK
 * \updated   October   (the) 15(th), 2019, 22:57 MSK
 * \TODO      
**/
#pragma once
#include <QtCore/qglobal.h>

#if defined(CATEGORY_DIALOG_LIBRARY)
#  define CATEGORY_DIALOG_SHARED_EXPORT Q_DECL_EXPORT
#else
#  define CATEGORY_DIALOG_SHARED_EXPORT Q_DECL_IMPORT
#endif
