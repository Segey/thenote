/**
 * \file      theNote/dialogs/category/add_dialog.h 
 * \brief     The Add_dialog class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   October   (the) 15(th), 2019, 22:52 MSK
 * \updated   October   (the) 15(th), 2019, 22:52 MSK
 * \TODO      
**/
#pragma once
#include "dialog.h"

/** \namespace dialog::category */
namespace dialog::category {

class CATEGORY_DIALOG_SHARED_EXPORT AddDialog final: public Dialog {
public:
    using class_name = AddDialog;
    using inherited  = Dialog;

protected:
    virtual bool doIsChanged() const Q_DECL_OVERRIDE Q_DECL_FINAL;
    virtual bool doIsValid() const Q_DECL_OVERRIDE Q_DECL_FINAL;

public:
    using Dialog::Dialog;
};

} // end namespace dialog::category
