/**
 * \file      perfect/Records/SimpleRecords/SimpleRecordsManager/simple_records_manager_instance.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March (the) 30(th), 2016, 18:38 MSK
 * \updated   March (the) 30(th), 2016, 18:38 MSK
 * \TODO      
**/
#pragma once
#include "RecordsLib/records_manager_instance.h"

/** \namespace records::simple */
namespace records {
namespace simple {

template<typename T, typename U>
class ManagerInstance: public records::ManagerInstance<T, U> {
public:
    using class_name = ManagerInstance<T,U>;
    using inherited  = records::ManagerInstance<T,U>;

private:
    void doTooltipTranslate() Q_DECL_OVERRIDE {
        inherited::ui->dock->setWindowTitle(iTitle::tr("Workout days"));
        inherited::ui->m_remove_all->setText(iToolTip::tr("Removes all records"));

#ifndef QT_NO_TOOLTIP
        inherited::ui->m_add->setToolTip(iToolTip::tr("Adds a record day"));
        inherited::ui->m_edit->setToolTip(iToolTip::tr("Edit the record day"));
        inherited::ui->m_remove->setToolTip(iToolTip::tr("Deletes the record day"));
        inherited::ui->m_remove_all->setToolTip(iToolTip::tr("Removes all records"));
#endif // QT_NO_TOOLTIP
    }
};
}} // end namespace records::simple
