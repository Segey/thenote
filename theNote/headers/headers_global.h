/**
 * \file      perfect/Headers/headers_global.h
 * \brief     The Headers_global class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2018
 * \version   v.1.0
 * \created   February  (the) 06(th), 2017, 16:00 MSK
 * \updated   February  (the) 06(th), 2017, 16:00 MSK
 * \TODO      
**/
#pragma once
#include <QtCore/qglobal.h>

#if defined(HEADERS_LIBRARY)
    #define HEADERS_SHARED_EXPORT Q_DECL_EXPORT
#else
    #define HEADERS_SHARED_EXPORT Q_DECL_IMPORT
#endif
