/**
 * \file      c:/projects/thenote/theNote/sources/instance/dialog.h 
 * \brief     The Dialog class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   October   (the) 18(th), 2019, 00:23 MSK
 * \updated   October   (the) 18(th), 2019, 00:23 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <QPixmap>
#include <ps2/qt/instance4/ui_dialog.h>
#include "headers/ui_dialog_head_panel.h"

/** \namespace the::instance  */
namespace the {
namespace instance {

/**
 * \last projects/Perfect/ExercisesGuide/EgExerciseDialogs/EgExerciseDialog/dialog_instance.h
 * <pre>
 *      #include <main/instance/dialog.h>
 *
 *      template <class UI, class T>
 *      class DialogInstance: public the::instance::Dialog<UI, T> {
 *      public:
 *          using class_name = DialogInstance<UI, T>;
 *          using inherited2 = instance::Dialog<UI, T>;
 *
 *      private:
 *          virtual void doInstanceForm() noexcept override final { }
 *          virtual void doInstanceWidgets() noexcept override final { }
 *          virtual void doInstanceLayouts() noexcept override final {}
 *          virtual void doTranslate() noexcept override final{}
 *          virtual void doSetBuddy() noexcept override final{}
 *          virtual void doConnect() noexcept override final{}
 *
 *      public:
 *          virtual ~DialogInstance() Q_DECL_EQ_DEFAULT;
 *      };
 *      }} // end namespace eg::exercise
 *
 * </pre>
 * \code
 *    instance::Dialog<Instance>(this, QStringLiteral("Cool"))();
 * \endcode
**/
template<class UI, class T>
class Dialog: public ps2::instance4::UiDialog<UI, T> {
public:
    using class_name = Dialog<UI, T>;
    using inherited  = ps2::instance4::UiDialog<UI, T>;
    using button_t   = typename inherited::button_t;

protected:
    void createHead(QPixmap const& pixmap, QString const& caption, QString const& title) noexcept {
        auto&& head = ui::DialogHeadPanel(pixmap, caption, title);
        head.instance(inherited::ui->m_head);
    }
};

}} // end namespace the::instance
